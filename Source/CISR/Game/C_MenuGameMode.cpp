// ForgottenGods

#include "C_MenuGameMode.h"
#include "C_GameHUD.h"
#include "C_CISRCharacter.h"

AC_MenuGameMode::AC_MenuGameMode()
{
    PlayerControllerClass = APlayerController::StaticClass();
    DefaultPawnClass = AC_CISRCharacter::StaticClass();
    HUDClass = AC_GameHUD::StaticClass();
}

void AC_MenuGameMode::StartPlay()
{
    Super::StartPlay();
    
    if (!GetWorld() 
        || !GetWorld()->GetFirstPlayerController())
    {
        return;
    }

    const auto PlayerConroller = Cast<APlayerController>(GetWorld()->GetFirstPlayerController());
    PlayerConroller->SetInputMode(FInputModeUIOnly());
    PlayerConroller->bShowMouseCursor = true;
}