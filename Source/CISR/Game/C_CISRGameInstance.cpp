#include "C_CISRGameInstance.h"

FLevelData UC_CISRGameInstance::GetStartupLevel() const
{
    if (StartupLevel.LevelName == "")
    {
        if (LevelsData[0].LevelName != "")
        {
            return LevelsData[0];
        }
    }
    else
    {
        return StartupLevel;
    }

    FLevelData DefaultLevelData;
    DefaultLevelData.LevelName = MenuLevelName;

    return DefaultLevelData;
}

void UC_CISRGameInstance::SetStartupLevel(const FLevelData& StartupLevelData)
{
    StartupLevel = StartupLevelData;
}

TArray<FLevelData> UC_CISRGameInstance::GetLevelsData() const
{
    return LevelsData;
}

FName UC_CISRGameInstance::GetMenuLevelName() const
{
    return MenuLevelName;
}

void UC_CISRGameInstance::SetSkipHistory()
{
    bSkipHistory = true;
}

bool UC_CISRGameInstance::GetSkipHistory()
{
    return bSkipHistory;
}
