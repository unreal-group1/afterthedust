// ForgottenGods

#include "C_CISRPlayerController.h"
#include "C_CISRGameMode.h"

#include "Components/AudioComponent.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "InputTriggers.h"

AC_CISRPlayerController::AC_CISRPlayerController()
{
    CreateMappingContext();

    AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
}

void AC_CISRPlayerController::BeginPlay()
{
    Super::BeginPlay();

    if (!GetWorld())
    {
        return;
    }

    const auto GameMode = Cast<AC_CISRGameMode>(GetWorld()->GetAuthGameMode());
    if (!GameMode)
    {
        return;
    }
    GameMode->OnGameStateChanged.AddUObject(this, &AC_CISRPlayerController::OnGameStateChanged);
}

void AC_CISRPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent);
    if (!EnhancedInputComponent)
    {
        return;
    }
    EnhancedInputComponent->BindAction(GamePauseAction, 
        ETriggerEvent::Triggered, 
        this, 
        &AC_CISRPlayerController::OnGamePause);
}

void AC_CISRPlayerController::CreateMappingContext()
{
    auto LocalPlayer = GetLocalPlayer();
    UEnhancedInputLocalPlayerSubsystem* EnhancedInputSubsystem =
        ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(LocalPlayer);
    if (!EnhancedInputSubsystem)
    {
        return;
    }

    EnhancedInputSubsystem->AddMappingContext(PlayerMappingContext, 0);
}

void AC_CISRPlayerController::OnGamePause()
{
    if (!GetWorld() 
        || !GetWorld()->GetAuthGameMode())
    {
        return;
    }

    GetWorld()->GetAuthGameMode()->SetPause(this);
}

void AC_CISRPlayerController::OnGameStateChanged(EGameState State)
{
    if (State == EGameState::Game)
    {
        SetInputMode(FInputModeGameOnly());
        bShowMouseCursor = false;
    }
    else
    {
        SetInputMode(FInputModeUIOnly());
        bShowMouseCursor = true;
    }
}
