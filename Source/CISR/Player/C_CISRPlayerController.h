// ForgottenGods

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "C_GameFunctionLibrary.h"
#include "C_CISRPlayerController.generated.h"

class UInputMappingContext;

UCLASS()
class CISR_API AC_CISRPlayerController : public APlayerController
{
	GENERATED_BODY()
	
	public:
    AC_CISRPlayerController();
    
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Audio)
    class UAudioComponent* AudioComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
    class UInputAction* GamePauseAction;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
    UInputMappingContext* PlayerMappingContext;

    virtual void BeginPlay() override;
    virtual void SetupInputComponent() override;
    void CreateMappingContext();

private:
    void OnGamePause();
    void OnGameStateChanged(EGameState State);
};
